// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faComments, faSitemap, faCameraRetro, faDownload, faShieldAlt, faPlayCircle, faTv, faFilm, faCloudDownloadAlt, faPlus, faChartPie, faMusic, faWifi, faTachometerAlt, faGlobe, faTshirt, faTable, faCloud } from '@fortawesome/free-solid-svg-icons'
import { faDocker } from '@fortawesome/free-brands-svg-icons/faDocker'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import BootstrapVue from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

library.add(faDownload)
library.add(faPlayCircle)
library.add(faSitemap)
library.add(faTv)
library.add(faFilm)
library.add(faCloudDownloadAlt)
library.add(faPlus)
library.add(faChartPie)
library.add(faMusic)
library.add(faWifi)
library.add(faTachometerAlt)
library.add(faGlobe)
library.add(faTshirt)
library.add(faTable)
library.add(faCloud)
library.add(faShieldAlt)
library.add(faDocker)
library.add(faComments)
library.add(faCameraRetro)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(BootstrapVue)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
})
